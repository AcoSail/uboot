# uboot

AcoSail uboot

## changelog

### v0.2
 - add rt-kernel
 - add nfs install instruction

### v0.3 
 - 增加CMA支持
 - 增加pcie的两个接口
 - 增加/dev/mem

### v1.0-RC
 - RT功能增强
 - 修改initramfs加载地址
 - headers与UEFI版本统一

### v1.0-RC-1
 - kernel与initramfs 支持 nfs启动

### v1.0-RC-2
 - 放开devmem访问限制